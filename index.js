// Importing data from the "data-1.js" file 
const got = require("./data-1");

// Function to count all people across all houses
function countAllPeople() {
  // Initializing totalPeople to 0 and using reduce to sum up the number of people in each house
  let totalPeople = got.houses.reduce((acc, house) => {
    // Counting the number of people in the current house
    let peopleCount = house.people.length;

    // Adding the people count of the current house to the accumulator
    acc += peopleCount;

    return acc;
  }, 0);

  // Returning the total count of people
  return totalPeople;
}

// Function to get the count of people per house
function peopleByHouses() {
  // Initializing an empty object to store the count of people per house
  let peoplePerHouse = got.houses.reduce((acc, house) => {
    // Counting the number of people in the current house
    let peopleCount = house.people.length;

    // Assigning an array containing the people count to the current house in the accumulator
    acc[house.name] = [peopleCount];

    return acc;
  }, {});

  // Returning the object with the count of people per house
  return peoplePerHouse;
}

// Function to get an array of all people's names across all houses
function everyone() {
  // Initializing an empty array to store the names of all people
  let allPeople = got.houses.reduce((acc, house) => {
    // Extracting the names of people in the current house and adding them to the accumulator
    let housePeople = house.people.map((person) => {
      return person.name;
    });

    // Concatenating the names of people in the current house to the accumulator
    acc.push(...housePeople);

    return acc;
  }, []);

  // Returning the array containing names of all people
  return allPeople;
}

// Function to get names of people containing the letter 'S'
function nameWithS() {
  // Initializing an empty array to store the names of people with the letter 'S' in their names
  let peopleWithS = got.houses.reduce((acc, house) => {
    // Filtering people in the current house whose names contain the letter 'S'
    let housePeople = house.people
      .filter((person) => {
        let name = person.name.toLowerCase();
        if (name.includes("s")) {
          return true;
        }
      })
      .map((person) => {
        return person.name;
      });

    // Concatenating the names of people in the current house to the accumulator
    acc.push(...housePeople);

    return acc;
  }, []);

  // Returning the array containing names of people with the letter 'S'
  return peopleWithS;
}

// Function to get names of people containing the letter 'A'
function nameWithA() {
  // Initializing an empty array to store the names of people with the letter 'A' in their names
  let peopleWithA = got.houses.reduce((acc, house) => {
    // Filtering people in the current house whose names contain the letter 'A'
    let housePeople = house.people
      .filter((person) => {
        let name = person.name.toLowerCase();
        if (name.includes("a")) {
          return true;
        }
      })
      .map((person) => {
        return person.name;
      });

    // Concatenating the names of people in the current house to the accumulator
    acc.push(...housePeople);

    return acc;
  }, []);

  // Returning the array containing names of people with the letter 'A'
  return peopleWithA;
}

// Function to get names of people with a surname starting with 'S'
function surnameWithS() {
  // Initializing an empty array to store the names of people with surnames starting with 'S'
  let surnameStartingS = got.houses.reduce((acc, house) => {
    // Filtering people in the current house whose surnames start with 'S'
    let housePeople = house.people
      .filter((person) => {
        let surname = person.name.split(" ")[1];
        if (surname.startsWith("S")) {
          return true;
        }
      })
      .map((person) => {
        return person.name;
      });

    // Concatenating the names of people in the current house to the accumulator
    acc.push(...housePeople);

    return acc;
  }, []);

  // Returning the array containing names of people with surnames starting with 'S'
  return surnameStartingS;
}

// Function to get names of people with a surname starting with 'A'
function surnameWithA() {
  // Initializing an empty array to store the names of people with surnames starting with 'A'
  let surnameStartingA = got.houses.reduce((acc, house) => {
    // Filtering people in the current house whose surnames start with 'A'
    let housePeople = house.people
      .filter((person) => {
        let surname = person.name.split(" ")[1];
        if (surname.startsWith("A")) {
          return true;
        }
      })
      .map((person) => {
        return person.name;
      });

    // Concatenating the names of people in the current house to the accumulator
    acc.push(...housePeople);

    return acc;
  }, []);

  // Returning the array containing names of people with surnames starting with 'A'
  return surnameStartingA;
}

// Function to get names of people for each house
function peopleNameOfAllHouses() {
  // Initializing an empty object to store the names of people for each house
  let peopleByHouse = got.houses.reduce((acc, house) => {
    // Extracting the names of people in the current house and assigning them to the current house in the accumulator
    let peopleOfHouse = house.people.map((person) => {
      return person.name;
    });

    // Assigning the names of people for the current house to the accumulator
    acc[house.name] = peopleOfHouse;

    return acc;
  }, {});

  // Returning the object containing names of people for each house
  return peopleByHouse;
}

// Invoking the functions and logging the results to the console
let totalPeople = countAllPeople();
let peoplePerHouse = peopleByHouses();
let allPeople = everyone();
let peopleWithS = nameWithS();
let peopleWithA = nameWithA();
let surnameStartingS = surnameWithS();
let surnameStartingA = surnameWithA();
let peopleByHouse = peopleNameOfAllHouses();

// Logging the results to the console
console.log(totalPeople);
console.log(peoplePerHouse);
console.log(allPeople);
console.log(peopleWithS);
console.log(peopleWithA);
console.log(surnameStartingS);
console.log(surnameStartingA);
console.log(peopleByHouse);
